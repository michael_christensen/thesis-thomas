package interfaces;

import java.util.List;

import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IBasicClient;

/**
 * All RESTful clients must be an interface which extends IBasicClient
 * This interface is not used
 */
public interface IRestfulClient extends IBasicClient {
 
    @Read()
    public Patient getResourceById(@IdParam IdDt theId);

    @Search()
    public List<Patient> getPatient(@RequiredParam(name = Patient.SP_FAMILY) StringDt theFamilyName);
    
    @Create()
    public MethodOutcome createPatient(@ResourceParam Patient thePatient);
    /*
    @Read()
    public Observation getLastObservation(@IdParam IdDt theId);
    
    @Search()
    public List<Observation> getObservations(@RequiredParam(name = Patient.SP_RES_ID) StringDt theId);
    
    @Create()
    public MethodOutcome createObservation(@ResourceParam Observation observation);
    */
}