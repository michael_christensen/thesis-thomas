package provider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.composite.HumanNameDt;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.dstu2.valueset.BundleTypeEnum;
import ca.uhn.fhir.model.dstu2.valueset.HTTPVerbEnum;
import ca.uhn.fhir.model.dstu2.valueset.ObservationStatusEnum;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.primitive.UriDt;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import utility.UtilityProduer;

public class RestfulBundleResourceProvider implements IResourceProvider {
	private long myNextId = 1;
	private Map<IdDt, Bundle> patientObservationList = new HashMap<IdDt, Bundle>();
	private IdDt tempId;
	private Bundle tempBundle;
	FhirContext ctx = FhirContext.forDstu2();
	private final String topic = "testBundle";
	private UtilityProduer util = new UtilityProduer();
	
    /**
     * The getResourceType method comes from IResourceProvider, and must
     * be overridden to indicate what type of resource this provider
     * supplies.
     */
    public Class<Bundle> getResourceType() {
        return Bundle.class;
    }
    
    public RestfulBundleResourceProvider() {
    	long id = myNextId;
        myNextId++;
    	
        Patient patient = new Patient();
        patient.addIdentifier();
        patient.getIdentifier().get(0).setSystem(new UriDt("urn:hapitest:mrns"));
        patient.getIdentifier().get(0).setValue("00001");
        patient.addName().addFamily("Test1");
        patient.getName().get(0).addGiven("Patient1");
        patient.setGender(AdministrativeGenderEnum.FEMALE);
        patient.setId(IdDt.newRandomUuid());
        
        
        // Create an observation object
        Observation observation = new Observation();
        observation.setStatus(ObservationStatusEnum.FINAL);
        observation
           .getCode()
              .addCoding()
                 .setSystem("http://loinc.org")
                 .setCode("789-8")
                 .setDisplay("Erythrocytes [#/volume] in Blood by Automated count");
        observation.setValue(
           new QuantityDt()
              .setValue(4.12)
              .setUnit("10 trillion/L")
              .setSystem("http://unitsofmeasure.org")
              .setCode("10*12/L"));

        // The observation refers to the patient using the ID, which is already
        // set to a temporary UUID  
        observation.setSubject(new ResourceReferenceDt(patient.getId().getValue()));

        // Create a bundle that will be used as a transaction
        Bundle bundle = new Bundle();
        bundle.setType(BundleTypeEnum.TRANSACTION);
        
        // Add the patient as an entry. This entry is a POST with an 
        // If-None-Exist header (conditional create) meaning that it
        // will only be created if there isn't already a Patient with
        // the identifier 12345
        bundle.addEntry()
           .setFullUrl(patient.getId().getValue())
           .setResource(patient)
           .getRequest()
              .setUrl("Patient")
              .setIfNoneExist("Patient?identifier=http://acme.org/mrns|12345")
              .setMethod(HTTPVerbEnum.POST);
        
        // Add the observation. This entry is a POST with no header
        // (normal create) meaning that it will be created even if
        // a similar resource already exists.
        bundle.addEntry()
           .setResource(observation)
           .getRequest()
              .setUrl("Observation")
              .setMethod(HTTPVerbEnum.POST);
        
        tempBundle = bundle;
        tempId = new IdDt(id);
        tempBundle.setId(tempId);
        patientObservationList.put(tempId, tempBundle);
    }
    
    @Read()
    public Bundle getResourceById(@IdParam IdDt theId) {
		System.out.println("------------------------");
		System.out.println("Trying id: " + theId.getIdPartAsLong());
		System.out.println("Keyset: " + patientObservationList.keySet().toString());
		System.out.println("------------------------");
		return patientObservationList.get(new IdDt(theId.getIdPartAsLong()));
    }
    
	@Create()
	public MethodOutcome createBundle(@ResourceParam Bundle theBundle) {
		long id = myNextId;
		myNextId++;
		System.out.println("Bundle resource name: " + theBundle.getResourceName());
        tempId = new IdDt(id);
        
        theBundle.getEntry().get(0).getResource().setId(IdDt.newRandomUuid());
        theBundle.setId(tempId);
        
		patientObservationList.put(tempId, theBundle);

		String encoded = ctx.newXmlParser().encodeResourceToString(theBundle);
		util.produceMessage(encoded, topic);

		return new MethodOutcome(new IdDt(id));
	}
	
	@Search()
	public List<Bundle> getPatients(@RequiredParam(name = Patient.SP_FAMILY) StringParam theFamilyName) {
		List<Bundle> temp = new ArrayList<Bundle>();
		System.out.println("StringDt trying: " + theFamilyName);
		String actualName = theFamilyName.getValue();
		for (IdDt id : patientObservationList.keySet()) {
			Bundle b = patientObservationList.get(id);
			Patient p = (Patient) b.getEntry().get(0).getResource();
			System.out.println(p.getId());
			LOOP: for (HumanNameDt name : p.getName()) {
				for (StringDt familyName : name.getFamily()) {
					if (actualName.equals(familyName.getValue()))  {
						temp.add(b);
						break LOOP;
					}
				}
			}
		}
		return temp;
	}
    
    /*
     
    @Read()
    public Patient getResourceById(@IdParam IdDt theId) {
    	System.out.println("------------------------");
    	System.out.println("Trying id: " + theId.getIdPartAsLong());
    	System.out.println("Actual id: " + temp.getId());
    	System.out.println("Keyset: " + patientList.keySet().toString());
    	System.out.println("Name: " + patientList.get(new IdDt(theId.getIdPartAsLong())).getName());
    	System.out.println("------------------------");
    	return patientList.get(new IdDt(theId.getIdPartAsLong()));
    }
    
    @Search()
    public List<Patient> getPatient(@RequiredParam(name = Patient.SP_FAMILY) StringParam theFamilyName) {
    	List<Patient> temp = new ArrayList<Patient>();
    	System.out.println("StringDt trying: " + theFamilyName);
    	String actualName = theFamilyName.getValue();
    	for (IdDt id : patientList.keySet()) {
    		Patient p = patientList.get(id);
    		LOOP: for (HumanNameDt name : p.getName()) {
    			for (StringDt familyName : name.getFamily()) {
    				if (actualName.equals(familyName.getValue()))  {
    					temp.add(p);
    					break LOOP;
    				}
    			}
    		}
    	}
    	return temp;
    }
    */

}
