package provider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import ca.uhn.fhir.model.dstu2.composite.HumanNameDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.valueset.*;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.primitive.UriDt;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import utility.UtilityProduer;
import ca.uhn.fhir.context.FhirContext;

/**
 * All resource providers must implement IResourceProvider
 */
public class RestfulPatientResourceProvider implements IResourceProvider {
	private long myNextId = 1;
	private Map<IdDt, Patient> patientList = new HashMap<IdDt, Patient>();
	private Map<IdDt, ArrayList<Observation>> observationList = new HashMap<IdDt, ArrayList<Observation>>();
	private Patient temp;
	private IdDt tempId;
	FhirContext ctx = FhirContext.forDstu2();
	private Properties props = new Properties();
	private final String topic = "testPatient";
	private UtilityProduer util = new UtilityProduer();

	/**
	 * The getResourceType method comes from IResourceProvider, and must
	 * be overridden to indicate what type of resource this provider
	 * supplies.
	 */
	public Class<Patient> getResourceType() {
		return Patient.class;
	}

	public RestfulPatientResourceProvider() {

		long id = myNextId;
		myNextId++;

		Patient patient = new Patient();
		patient.setId(Long.toString(id));
		patient.addIdentifier();
		patient.getIdentifier().get(0).setSystem(new UriDt("urn:hapitest:mrns"));
		patient.getIdentifier().get(0).setValue("00001");
		patient.addName().addFamily("Test1");
		patient.getName().get(0).addGiven("Patient1");
		patient.setGender(AdministrativeGenderEnum.FEMALE);

		patientList.put(patient.getId(), patient);
		temp = patient;
		tempId = patient.getId();

		props = util.getProperties();
	}

	@Read()
	public Patient getResourceById(@IdParam IdDt theId) {
		System.out.println("------------------------");
		System.out.println("Trying id: " + theId.getIdPartAsLong());
		System.out.println("Actual id: " + temp.getId());
		System.out.println("Keyset: " + patientList.keySet().toString());
		System.out.println("Name: " + patientList.get(new IdDt(theId.getIdPartAsLong())).getName());
		System.out.println("------------------------");
		return patientList.get(new IdDt(theId.getIdPartAsLong()));
	}

	@Search()
	public List<Patient> getPatient(@RequiredParam(name = Patient.SP_FAMILY) StringParam theFamilyName) {
		List<Patient> temp = new ArrayList<Patient>();
		System.out.println("StringDt trying: " + theFamilyName);
		String actualName = theFamilyName.getValue();
		for (IdDt id : patientList.keySet()) {
			Patient p = patientList.get(id);
			LOOP: for (HumanNameDt name : p.getName()) {
				for (StringDt familyName : name.getFamily()) {
					if (actualName.equals(familyName.getValue()))  {
						temp.add(p);
						break LOOP;
					}
				}
			}
		}
		return temp;
	}


	@Create()
	public MethodOutcome createPatient(@ResourceParam Patient thePatient) {
		long id = myNextId;
		myNextId++;
		System.out.println("Patient names: " + thePatient.getName());
		Patient p = thePatient;
		p.setId(Long.toString(id));
		patientList.put(p.getId(), p);

		String encoded = ctx.newXmlParser().encodeResourceToString(p);
		util.produceMessage(encoded, topic);

		return new MethodOutcome(new IdDt(id));
	}


	/*
    @Read()
    public Observation getLastObservation(@IdParam IdDt theId) {
    	Observation temp = new Observation();
    	IdDt properId = new IdDt(theId.getIdPartAsLong());

    	if (observationList.get(properId) != null) {
    		temp = observationList.get(properId).get(observationList.size() - 1);
    	} else {
    		//throw new ResourceNotFoundException(properId);
    	}
		return temp;	
    }


    @Search()
    public List<Observation> getObservations(@RequiredParam(name = Patient.SP_RES_ID) IdDt theId) {
    	ArrayList<Observation> tempList = new ArrayList<Observation>();
    	IdDt properId = new IdDt(theId.getIdPartAsLong());
    	if (observationList.get(properId) != null) {
    		tempList = observationList.get(properId);
    	} else {
    		throw new ResourceNotFoundException(properId);
    	}
		return tempList;
    }

    @Create(type=Observation.class)
    public MethodOutcome createObservation(@ResourceParam Observation observation) {
    	IdDt properId = new IdDt(1);
    	Patient patient = getResourceById(properId);
    	ArrayList<Observation> tempList = new ArrayList<Observation>();
    	System.out.println("-----------------------------------------");
    	System.out.println("Trying id: " + properId);
    	System.out.println("Patient: " + patient.getName());
    	System.out.println("Observation : " + observation.getStatus());
    	System.out.println("-----------------------------------------");
    	if (observationList.get(properId) != null) {
			tempList = observationList.get(properId);
			tempList.add(observation);
			observationList.put(properId, tempList);
		} else {
			tempList.add(observation);
			observationList.put(properId, tempList);
		}
		return new MethodOutcome(properId);
    }
	 */
}
