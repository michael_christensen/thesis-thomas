package servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import provider.RestfulBundleResourceProvider;
import provider.RestfulObservationResourceProvider;
import provider.RestfulPatientResourceProvider;
import utility.UtilityProduer;

/**
 * In this example, we are using Servlet 3.0 annotations to define
 * the URL pattern for this servlet, but we could also
 * define this in a web.xml file.
 */
@WebServlet("/thesis-server/fhir/*")
public class ExampleRestfulServlet extends RestfulServer {

	private static final long serialVersionUID = 1L;


	/**
	 * Constructor
	 */
	public ExampleRestfulServlet() {
		super(FhirContext.forDstu2()); // Support DSTU2
	}

	/**
	 * The initialize method is automatically called when the servlet is starting up, so it can
	 * be used to configure the servlet to define resource providers, or set up
	 * configuration, interceptors, etc.
	 */
	@Override
	protected void initialize() throws ServletException {
		/*
		 * The servlet defines any number of resource providers, and
		 * configures itself to use them by calling
		 * setResourceProviders()
		 */

		List<IResourceProvider> resourceProviders = new ArrayList<IResourceProvider>();
		resourceProviders.add(new RestfulPatientResourceProvider());
		resourceProviders.add(new RestfulObservationResourceProvider());
		resourceProviders.add(new RestfulBundleResourceProvider());
		setResourceProviders(resourceProviders);

		registerInterceptor(new ResponseHighlighterInterceptor());
		setDefaultPrettyPrint(true);
	}

}
