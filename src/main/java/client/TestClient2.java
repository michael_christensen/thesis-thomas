package client;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.client.IGenericClient;

public class TestClient2 {

	public static void main(String[] args) {
		FhirContext ctx = FhirContext.forDstu2();
		String serverBase = "http://localhost:8080/thesis-server/fhir/";

		// Create the client
		//IRestfulClient client = ctx.newRestfulClient(IRestfulClient.class, serverBase);
		IGenericClient client = ctx.newRestfulGenericClient(serverBase); 

		// Try the client out! This method will invoke the server
		Patient patient = new Patient();
		IdDt id = new IdDt(Long.toString(1));


	}
}
