package client;

import java.util.*;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;


public class TestKafkaConsumer {
	private static final String topicPatient = "testPatient";
	private static final String topicObservation = "testObservation";
	private static final String topicBundle = "testBundle";
	private static final String patientIdentifier = "<Patient xmlns=\"http://hl7.org/fhir\">";
	private static final String observationIdentifier = "<Observation xmlns=\"http://hl7.org/fhir\">";
	private static final String bundleIdentifier = "<Bundle xmlns=\"http://hl7.org/fhir\">";
	private static KafkaConsumer<String, String> consumer;
	private static Map<Long, String> observationMap = new HashMap<Long, String>();
	private static Map<Long, String> patientMap = new HashMap<Long, String>();
	private static Map<Long, String> bundleMap = new HashMap<Long, String>();

	public static void main(String[] args) throws InterruptedException {
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("zookeeper.connect", "localhost:2181");
	    props.put("auto.offset.reset", "earliest"); //Use latest if you don't want previous
		props.put("enable.auto.commit", "false");
		props.put("group.id", "test-consumer-group");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		try {
			consumer = new KafkaConsumer<>(props);
			consumer.subscribe(Arrays.asList(topicPatient, topicObservation, topicBundle));
			//consumer.seek(Arrays.asList(topicPatient, topicObservation), 15);
			while (true) {
				ConsumerRecords<String, String> records = consumer.poll(1000);
				for (ConsumerRecord<String, String> record : records)  {
					String tempValue = record.value();
					long tempOffset = record.offset();
					if (record.value().contains(bundleIdentifier)) {
						bundleMap.put(record.offset(), record.value());
						System.out.printf("Bundle value: " + tempValue + "\n");
						System.out.printf("Offset: " + tempOffset + "\n");
					} else if (record.value().contains(patientIdentifier)) {
						patientMap.put(record.offset(), record.value());
						System.out.printf("Patient value: " + tempValue + "\n");
						System.out.printf("Offset: " + tempOffset + "\n");
					} else if (record.value().contains(observationIdentifier)) {
						observationMap.put(record.offset(), record.value());
						System.out.printf("Observation value: " + tempValue + "\n");
						System.out.printf("Offset: " + tempOffset + "\n");
					} else {
						System.out.println("Nothing matched");
					}
					
					
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			consumer.close();
		}


		/*
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("zookeeper.connect", "localhost:2181");
		props.put("group.id", "test-consumer-id");
		props.put("enable.auto.commit", "false");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Arrays.asList("test"));
		//while (true) {
		System.out.println("And here");
		ConsumerRecords<String, String> records = consumer.poll(100);
		System.out.println("I'm here:");
		for (ConsumerRecord<String, String> record : records) {
			System.out.printf(record.value());
		}
		System.out.println("Also here");
		consumer.close();
		//}
		 * */

		/*
		Properties props = new Properties();
		props.put("zookeeper.connect", "localhost:2181");
		props.put("group.id", "test-consumer-id");
		props.put("enable.auto.commit", "false");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "30000");
		consumer = Consumer.createJavaConsumerConnector(new ConsumerConfig(props));

		Map<String, Integer> topicCount = new HashMap<>();
		topicCount.put(topic, 1);


		Map<String, List<KafkaStream<byte[], byte[]>>> consumerStreams = consumer.createMessageStreams(topicCount);
		List<KafkaStream<byte[], byte[]>> streams = consumerStreams.get(topic);
		for (final KafkaStream stream : streams) {
			ConsumerIterator<byte[], byte[]> it = stream.iterator();
			while (it.hasNext()) {
				System.out.println("Message from Single Topic: " + new String(it.next().message()));
			}
		}
		if (consumer != null) {
			consumer.shutdown();
		}
		 */
	}
}
