package client;

import java.util.ArrayList;
import java.util.List;

import ca.uhn.fhir.context.FhirContext;
//import ca.uhn.fhir.model.api.Bundle;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.dstu2.valueset.BundleTypeEnum;
import ca.uhn.fhir.model.dstu2.valueset.HTTPVerbEnum;
import ca.uhn.fhir.model.dstu2.valueset.ObservationStatusEnum;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.primitive.UriDt;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.client.api.IRestfulClient;
import ca.uhn.fhir.rest.method.CreateMethodBinding;

public class TestClient {

	public static void main(String[] args) {
		FhirContext ctx = FhirContext.forDstu2();
		String serverBase = "http://localhost:8080/thesis-server/fhir/";

		// Create the client
		//IRestfulClient client = ctx.newRestfulClient(IRestfulClient.class, serverBase);
		IGenericClient client = ctx.newRestfulGenericClient(serverBase); 

		// Try the client out! This method will invoke the server
		Patient patient = new Patient();
		IdDt id = new IdDt(Long.toString(1));

		Observation observation = new Observation();
		observation.setStatus(ObservationStatusEnum.FINAL);
		observation
		.getCode()
		.addCoding()
		.setSystem("http://loinc.org")
		.setCode("789-8")
		.setDisplay("Erythrocytes [#/volume] in Blood by Automated count");
		observation.setValue(
				new QuantityDt()
				.setValue(4.12)
				.setUnit("10 trillion/L")
				.setSystem("http://unitsofmeasure.org")
				.setCode("10*12/L"));

		/*
		 * Bundle results = client.search()
		 
				.forResource(Patient.class)
				.where(Patient.FAMILY.matches().value("Test2"))
				.execute();
				

		List<Patient> patients = results.getResources(Patient.class);
		for (Patient pat : patients) {
			System.out.println("Name: " + pat.getName());
		}
		*/


		Patient p = new Patient();
		p.addIdentifier();
		p.getIdentifier().get(0).setSystem(new UriDt("urn:hapitest:mrns"));
		p.getIdentifier().get(0).setValue("77777");
		p.addName().addFamily("Test2");
		p.getName().get(0).addGiven("PatientTwo");
		p.setGender(AdministrativeGenderEnum.MALE);

		/*
		MethodOutcome temp = client.create().resource(p).execute();
		System.out.println("Temp: " + temp.getId());

		Patient temp2 = client.read().resource(Patient.class).withId(id).execute();

		System.out.println("Temp2_ " + temp2.getName() + ", id: " + temp2.getId());


		MethodOutcome temp3 = client.create().resource(observation).execute();
		System.out.println("Temp3: " + temp3.getId());

		Observation temp4 = client.read().resource(Observation.class).withId(id).execute();
		System.out.println("Temp4: " + temp4.getCode().toString());
		*/
		
		Bundle bundle = createBundleTest();
		MethodOutcome temp5 = client.create().resource(bundle).execute();
		System.out.println("Temp5: " + temp5.getId());
		
		ca.uhn.fhir.model.api.Bundle bun = client.search()
							.forResource(Bundle.class)
							.where(Patient.FAMILY.matches()
							.value("Test8"))
							.execute();
		List<Bundle> list = bun.getResources(Bundle.class);
		for (Bundle b : list) {
			System.out.println("Bundle: " + b.getId());
		}

		//client.createObservation(observation);

		/*
	         Observation o = client.getLastObservation(id);


	        System.out.println("-----------------------------");
	        System.out.println("Observation value: " + o.getValue());
	        System.out.println("-----------------------------");

			   IParser parser = ctx.newXmlParser();
			   parser.setPrettyPrint(true);
			   String encode = parser.encodeResourceToString(o);
			   System.out.println(encode);
		 */



		/*
	       client.create()
	             		 .resource(patient)
	       		 .execute();
		 */




		/*
	       client.createPatient(p);



	       patient = client.getResourceById(id);
		   System.out.println("Found\nGender: " + patient.getGender() +"\nId: " + patient.getId() +"\nName: " + patient.getName());

		   IParser parser = ctx.newXmlParser();
		   parser.setPrettyPrint(true);
		   String encode = parser.encodeResourceToString(patient);
		   System.out.println(encode);



		   StringDt name = new StringDt("Test2");
		   List<Patient> patients = client.getPatient(name);
		   System.out.println("List of patients:\n");
		   System.out.println("Size: " + patients.size());
		   System.out.println(patients.get(0).getName());
		 */
	}
	
	private static Bundle createBundleTest() {
    	
        Patient patient = new Patient();
        patient.addIdentifier();
        patient.getIdentifier().get(0).setSystem(new UriDt("urn:hapitest:mrns"));
        patient.getIdentifier().get(0).setValue("00001");
        patient.addName().addFamily("Test100");
        patient.getName().get(0).addGiven("Patient5");
        patient.setGender(AdministrativeGenderEnum.FEMALE);
        
        patient.setId(IdDt.newRandomUuid());
        
        // Create an observation object
        Observation observation = new Observation();
        observation.setStatus(ObservationStatusEnum.FINAL);
        observation
           .getCode()
              .addCoding()
                 .setSystem("http://loinc.org")
                 .setCode("789-8")
                 .setDisplay("Erythrocytes [#/volume] in Blood by Automated count");
        observation.setValue(
           new QuantityDt()
              .setValue(4.12)
              .setUnit("10 trillion/L")
              .setSystem("http://unitsofmeasure.org")
              .setCode("10*12/L"));

        // The observation refers to the patient using the ID, which is already
        // set to a temporary UUID  
        observation.setSubject(new ResourceReferenceDt(patient.getId().getValue()));

        // Create a bundle that will be used as a transaction
        Bundle bundle = new Bundle();
        bundle.setType(BundleTypeEnum.TRANSACTION);
        
        // Add the patient as an entry. This entry is a POST with an 
        // If-None-Exist header (conditional create) meaning that it
        // will only be created if there isn't already a Patient with
        // the identifier 12345
        bundle.addEntry()
           .setFullUrl(patient.getId().getValue())
           .setResource(patient)
           .getRequest()
              .setUrl("Patient")
              .setIfNoneExist("Patient?identifier=http://acme.org/mrns|12345")
              .setMethod(HTTPVerbEnum.POST);
        
        // Add the observation. This entry is a POST with no header
        // (normal create) meaning that it will be created even if
        // a similar resource already exists.
        bundle.addEntry()
           .setResource(observation)
           .getRequest()
              .setUrl("Observation")
              .setMethod(HTTPVerbEnum.POST);
        
        return bundle;
	}

}
